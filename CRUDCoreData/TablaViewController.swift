//
//  TablaViewController.swift
//  CRUDCoreData
//
//  Created by Daniel on 01/02/2019.
//  Copyright © 2019 Daniel Gomez. All rights reserved.
//

import UIKit
import CoreData

class TablaViewController: UIViewController {
    
    @IBOutlet weak var tabla: UITableView!
    var personas: [Persona] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabla.reloadData()
        self.tabla.delegate = self
        self.tabla.dataSource = self
        self.mostrarDatos()
    }
    
    func conexion() -> NSManagedObjectContext {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        return delegate.persistentContainer.viewContext
    }
    
    // MARK: -
    func mostrarDatos() {
        let contexto = self.conexion()
        let fetchRequest: NSFetchRequest<Persona> = Persona.fetchRequest()
        
        do {
            self.personas = try contexto.fetch(fetchRequest)
        } catch let error as NSError {
            print("No hay registros cargados", error)
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "enviarEditar" {
            if let id = tabla.indexPathForSelectedRow {
                let fila = personas[id.row]
                let destino = segue.destination as! EditarViewController
                destino.personaEditar = fila
            }
        }
    }

}

extension TablaViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: -
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.personas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tabla.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let persona = self.personas[indexPath.row]
        
        if persona.estado {
            cell.textLabel?.text = "💚 \(persona.nombre!)"
        }else{
            cell.textLabel?.text = "❤️ \(persona.nombre!)"
        }
        cell.detailTextLabel?.text = "\(persona.edad)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let contexto = self.conexion()
        let persona = self.personas[indexPath.row]
        
        if editingStyle == .delete {
            contexto.delete(persona)
            
            do {
                try contexto.save()
                print("Eliminado correctamente")
            }catch let error as NSError {
                print("No se pudo eliminar")
            }
        }
        mostrarDatos()
        self.tabla.reloadData()
    }
}
