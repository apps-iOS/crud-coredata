//
//  ViewController.swift
//  CRUDCoreData
//
//  Created by Daniel Gomez on 21/01/2019.
//  Copyright © 2019 Daniel Gomez. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    @IBOutlet weak var textFieldNombre: UITextField!
    @IBOutlet weak var pickerEdad: UIPickerView!
    @IBOutlet weak var switchActivo: UISwitch!
    var picker: [String] = [String]()
    var edad: Int!
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pickerEdad.delegate = self
        self.pickerEdad.dataSource = self
        for i in 1...150 {
            self.picker.append(String(i))
        }
        self.reinicializar()
    }
    
    func conexion() -> NSManagedObjectContext {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        return delegate.persistentContainer.viewContext
    }
    
    func reinicializar() {
        self.edad = 1
        self.pickerEdad.selectRow(0, inComponent: 0, animated: true)
        self.textFieldNombre.text = ""
        self.switchActivo.isOn = false
        resignFirstResponder()
    }
    
    @IBAction func esconderTeclado(_ sender: Any) {
        print("Esconder teclado")
        //resignFirstResponder()
    }
    
    
    // MARK: -
    @IBAction func guardar(_ sender: UIButton) {
        
        let contexto = self.conexion()
        let entidadPersona = NSEntityDescription.entity(forEntityName: "Persona", in: contexto)
        let newPersona = NSManagedObject(entity: entidadPersona!, insertInto: contexto)
        
        newPersona.setValue(self.textFieldNombre.text, forKey: "nombre")
        newPersona.setValue(self.edad, forKey: "edad")
        newPersona.setValue(self.switchActivo.isOn, forKey: "estado")
        
        do {
            try contexto.save()
            print("Guardado correctamente.")
            self.reinicializar()
        } catch let error as NSError {
            print("No se pudo guardar.", error)
        }
    }
    
    @IBAction func mostrar(_ sender: UIButton) {
        let contexto = self.conexion()
        let fetchRequest : NSFetchRequest<Persona> = Persona.fetchRequest()
        
        do {
            let resultados = try contexto.fetch(fetchRequest)
            print("Cantidad de registros: \(resultados.count)")
            for resultado in resultados as [NSManagedObject] {
                let nombre = resultado.value(forKey: "nombre")
                let edad = resultado.value(forKey: "edad")
                let activo = resultado.value(forKey: "estado")
                print("Nombre: \(nombre!) - Edad: \(edad!) - Activo: \(activo!)")
            }
        } catch let error as NSError {
            print("No se pudo recuperar los datos.", error)
        }
    }
    
    @IBAction func borrar(_ sender: Any) {
        let contexto = self.conexion()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Persona")
        let borrar = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try contexto.execute(borrar)
            print("Datos borrados correctamente.")
        } catch let error as NSError {
            print("No se pudo borrar los datos.", error)
        }
    }
    
    
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    // MARK: -
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.picker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.picker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.edad = Int(self.picker[row])
    }
}

