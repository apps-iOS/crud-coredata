//
//  EditarViewController.swift
//  CRUDCoreData
//
//  Created by Daniel Gomez on 04/02/2019.
//  Copyright © 2019 Daniel Gomez. All rights reserved.
//

import UIKit
import CoreData

class EditarViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var personaEditar: Persona!
    @IBOutlet weak var textFieldNombre: UITextField!
    @IBOutlet weak var pickerEdad: UIPickerView!
    @IBOutlet weak var switchActivo: UISwitch!
    var picker: [String] = [String]()
    var edad: Int!
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pickerEdad.delegate = self
        self.pickerEdad.dataSource = self
        for i in 1...150 {
            self.picker.append(String(i))
        }
        self.textFieldNombre.text = self.personaEditar.nombre
        self.edad = Int(self.personaEditar.edad)
        self.pickerEdad.selectRow(self.edad-1, inComponent: 0, animated: true)
        self.switchActivo.isOn = self.personaEditar.estado
        
        print("EDAD: \(self.edad!)")
    }

    // MARK: -
    func conexion() -> NSManagedObjectContext {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        return delegate.persistentContainer.viewContext
    }
    
    @IBAction func guardar(_ sender: Any) {
        let contexto = self.conexion()
        personaEditar.setValue(self.textFieldNombre.text, forKey: "nombre")
        personaEditar.setValue(self.edad, forKey: "edad")
        personaEditar.setValue(self.switchActivo.isOn, forKey: "estado")
        
        do {
            try contexto.save()
            print("Editado correctamente.")
            performSegue(withIdentifier: "volverEditar", sender: self)
        } catch let error as NSError {
            print("No se pudo editar.", error)
        }
    }
    
    // MARK: -
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.picker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.picker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.edad = Int(self.picker[row])
    }
}
